package com.example.testing.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;

import android.view.Gravity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;

import com.example.testing.MainActivity;


public class UIUtils extends AppCompatActivity {

    public static Animation blinkAnimation = null;
    private static ProgressDialog progressDialog = null;
    final Activity activity = null;
    Context mContext;

    static
    {
        blinkAnimation = new AlphaAnimation(0.0f, 1.0f);
        //blinkAnimation.setDuration(Constant.HALF_SECOND);
        blinkAnimation.setStartOffset(0);
        blinkAnimation.setRepeatMode(Animation.REVERSE);
        blinkAnimation.setRepeatCount(Animation.INFINITE);
    }

    public static void NetworkError_message(String title, String mymessage,
                                            Activity activity)
    {
        UIUtils sms = new UIUtils();
        sms.NetworkError_function(title,mymessage,activity);

    }

    public void NetworkError_function(String title, String mymessage,
                                      final Activity activity)
    {
        //  final Activity activity=null;
        new android.app.AlertDialog.Builder(activity)
                .setMessage(mymessage)
                .setTitle(title)
                .setCancelable(false)
                .setNegativeButton(android.R.string.ok,
                        new android.content.DialogInterface.OnClickListener()
                        {
                            public void onClick(
                                    android.content.DialogInterface dialog,
                                    int whichButton)
                            {
                               // mContext=context;
                                AppManager.getInstance().addActivity(activity);
                                activity.startActivity(new Intent(activity, MainActivity.class));
                            //    dialog.setCanceledOnTouchOutside(false);
                                //Builder.setCancelable(false);
//                                activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
//                                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                AppManager.getInstance().finishActivity();
//                                Intent intent = new Intent(activity,MainActivity.class)
//                                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                activity.startActivity(intent);
                                //     System.exit(0);
                            }
                        }).show();

    }
    public static void Eggtypeupdate_message(String title, String mymessage,
                                             Activity activity)
    {
        UIUtils sms = new UIUtils();
        sms.NetworkError_function(title,mymessage,activity);
    }

    public void Eggtypeupdate_message_function(String title, String mymessage,
                                               final Activity activity)
    {
        //  final Activity activity=null;
        new android.app.AlertDialog.Builder(activity)
                .setMessage(mymessage)
                .setTitle(title)
                .setCancelable(true)
                .setNegativeButton(android.R.string.ok,
                        new android.content.DialogInterface.OnClickListener()
                        {
                            public void onClick(
                                    android.content.DialogInterface dialog,
                                    int whichButton)
                            {
                                // mContext=context;
                                AppManager.getInstance().addActivity(activity);
                                activity.startActivity(new Intent(activity,MainActivity.class));

                                AppManager.getInstance().finishActivity();
//                                Intent intent = new Intent(activity,MainActivity.class)
//                                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                activity.startActivity(intent);
                                //     System.exit(0);
                            }
                        }).show();

    }

    public static void messageBox(String title, String mymessage,
                                  Context context)
    {
      //  final Activity activity=null;
        new android.app.AlertDialog.Builder(context)
                .setMessage(mymessage)
                .setTitle(title)
                .setCancelable(true)
                .setNegativeButton(android.R.string.ok,
                        new android.content.DialogInterface.OnClickListener()
                        {
                            public void onClick(
                                    android.content.DialogInterface dialog,
                                    int whichButton)
                            {

                           //     System.exit(0);
                            }
                        }).show();

    }

    public static void messageBox(String title, String mymessage)
    {
        new android.app.AlertDialog.Builder(AppManager.getInstance()
                .getCurrentActivity())
                .setMessage(mymessage)
                .setTitle(title)
                .setCancelable(true)
                .setNegativeButton(android.R.string.ok,
                        new android.content.DialogInterface.OnClickListener()
                        {
                            public void onClick(
                                    android.content.DialogInterface dialog,
                                    int whichButton)
                            {
                                AppManager.getInstance().finishActivity();
                            }
                        }).show();

    }

    public static void toastMessage(String message)
    {
        Toast toast = Toast.makeText(AppManager.getInstance().getCurrentActivity(),
                message, Toast.LENGTH_LONG);
        LinearLayout toastLayout = (LinearLayout) toast.getView();
        TextView toastTV = (TextView) toastLayout.getChildAt(0);
        toastTV.setTextSize(20);
        toast.setGravity(Gravity.CENTER_HORIZONTAL| Gravity.CENTER_VERTICAL, 0, 0);
        toast.show();
    }

    public static void toastMessage(String message, Activity activity, int length)
    {
        Toast toast = Toast.makeText(activity, message, length);
        LinearLayout toastLayout = (LinearLayout) toast.getView();
        TextView toastTV = (TextView) toastLayout.getChildAt(0);
        toastTV.setTextSize(20);
        toast.setGravity(Gravity.CENTER_HORIZONTAL| Gravity.CENTER_VERTICAL, 0, 0);
        toast.show();
    }

    public static void updateConfirmedCountView(TextView textView,
                                                int unConfCount)
    {
        if (textView != null)
        {
            if (unConfCount > 0)
            {
                textView.setVisibility(View.VISIBLE);
                textView.setText(unConfCount + "");
                textView.startAnimation(blinkAnimation);
            }
            else
            {
                textView.clearAnimation();
                textView.setVisibility(View.GONE);
            }
        }
    }

    public static void showProgressDialog(Context context, String message)
    {
        progressDialog = new ProgressDialog(context);
      //  progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public static void dismissProgressDialog()
    {
        progressDialog.dismiss();
    }
    public static void clearDialog()
    {
        progressDialog = null;
    }

}
